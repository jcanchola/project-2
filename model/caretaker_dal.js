var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM caretaker;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(department_id, callback) {
    var query = 'SELECT * FROM department WHERE department_id = ?';
    var queryData = [department_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO caretaker (email, first_name, last_name) VALUES (?, ?, ?)';
    var queryData = [params.email, params.first_name, params.last_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(caretaker_id, callback ) {
    var query = 'DELETE FROM caretaker WHERE caretaker_id = ?';
    var queryData = [caretaker_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE caretaker SET email = ?, first_name = ?, last_name = ? WHERE caretaker_id = ?';
    var queryData = [params.email, params.first_name, params.last_name, params.caretaker_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = function(caretaker_id, callback) {
    var query = 'CALL caretaker_getinfo(?)';
    var queryData = [caretaker_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};