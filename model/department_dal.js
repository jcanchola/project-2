var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM department;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(animal_id, callback) {
    var query = 'SELECT * FROM animal WHERE animal_id = ?';
    var queryData = [animal_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO department (department_name) VALUES (?)';
    var queryData = [params.department_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(department_id, callback ) {
    var query = 'DELETE FROM department WHERE department_id = ?';
    var queryData = [department_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE department SET department_name = ?';
    var queryData = [params.department_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = exports.getById;