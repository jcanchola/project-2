var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM duty;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(duty_id, callback) {
    var query = 'SELECT * FROM duty WHERE duty_id = ?';
    var queryData = [duty_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO duty (duty_name, description) VALUES (?, ?)';
    var queryData = [params.duty_name, params.description];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(duty_id, callback ) {
    var query = 'DELETE FROM duty WHERE duty_id = ?';
    var queryData = [duty_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE duty SET duty_name = ?, description WHERE duty_id = ?';
    var queryData = [params.duty_name, params.description, params.duty_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = exports.getById;
