var express = require('express');
var router = express.Router();
var animal_dal = require('../model/animal_dal');

// View All animals
router.get('/all', function(req, res) {
    animal_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('animal/animalViewAll', { 'result':result });
        }
    });

});

// View the animal for the given id
router.get('/', function(req, res){
    if(req.query.animal_id == null) {
        res.send('animal_id is null');
    }
    else {
        animal_dal.getById(req.query.animal_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('animal/animalViewById', {'result': result});
            }
        });
    }
});

// Return the add a new department form

router.get('/add', function(req, res) {
    res.render('animal/animalAdd');
});

// View the animal for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.animal_name == null) {
        res.send('animal_name must be provided.');
    }
    else if(req.query.species == null) {
        res.send('Must enter species.');
    }
    else if(req.query.sex == null) {
        res.send('Must enter sex.');
    }
    else if(req.query.diet == null) {
        res.send('Must enter diet.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        animal_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differenty once we start using Ajax
                res.redirect(302, '/animal/all');
            }
        });
    }
});

router.get('/edit', function(req, res) {
    if(req.query.animal_id == null) {
        res.send('A animal id is required');
    }
    else {
        animal_dal.edit(req.query.animal_id, function(err, result) {
            res.render('animal/animalUpdate', {animal: result[0]});
        });
    }
});

router.get('/update', function(req, res) {
    animal_dal.update(req.query, function(err, result){
        res.redirect(302, '/animal/all');
    });
});

// Delete an department for the given department_id
router.get('/delete', function(req, res){
    if(req.query.animal_id == null) {
        res.send('animal_id is null');
    }
    else {
        animal_dal.delete(req.query.animal_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/animal/all');
            }
        });
    }
});

module.exports = router;