var express = require('express');
var router = express.Router();
var caretaker_dal = require('../model/caretaker_dal');

// View All departments
router.get('/all', function(req, res) {
    caretaker_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('caretaker/caretakerViewAll', { 'result':result });
        }
    });

});

// View the department for the given id
router.get('/', function(req, res){
    if(req.query.caretaker_id == null) {
        res.send('caretaker_id is null');
    }
    else {
        caretaker_dal.getById(req.query.caretaker_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('caretaker/caretakerViewById', {'result': result});
            }
        });
    }
});

// Return the add a new department form

router.get('/add', function(req, res) {
    res.render('caretaker/caretakerAdd');
});

// View the names for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.email == null) {
        res.send('Email must be provided.');
    }
    else if(req.query.first_name == null) {
        res.send('Must enter first name.');
    }
    else if(req.query.last_name == null) {
        res.send('Must enter last name.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        caretaker_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differenty once we start using Ajax
                res.redirect(302, '/caretaker/all');
            }
        });
    }
});

router.get('/edit', function(req, res) {
    if(req.query.caretaker_id == null) {
        res.send('A caretaker id is required');
    }
    else {
        caretaker_dal.edit(req.query.caretaker_id, function(err, result) {
            res.render('caretaker/caretakerUpdate', {caretaker: result[0]});
        });
    }
});

router.get('/update', function(req, res) {
    caretaker_dal.update(req.query, function(err, result){
        res.redirect(302, '/caretaker/all');
    });
});

// Delete an department for the given department_id
router.get('/delete', function(req, res){
    if(req.query.caretaker_id == null) {
        res.send('caretaker_id is null');
    }
    else {
        caretaker_dal.delete(req.query.caretaker_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/caretaker/all');
            }
        });
    }
});

module.exports = router;