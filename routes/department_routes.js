var express = require('express');
var router = express.Router();
var department_dal = require('../model/department_dal');

// View All departments
router.get('/all', function(req, res) {
    department_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('department/departmentViewAll', { 'result':result });
        }
    });

});

// View the department for the given id
router.get('/', function(req, res){
    if(req.query.department_id == null) {
        res.send('department_id is null');
    }
    else {
        department_dal.getById(req.query.department_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('department/departmentViewById', {'result': result});
            }
        });
    }
});

// Return the add a new department form

router.get('/add', function(req, res) {
    res.render('department/departmentAdd');
});

// View the department for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.department_name == null) {
        res.send('Department must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        department_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differenty once we start using Ajax
                res.redirect(302, '/department/all');
            }
        });
    }
});

router.get('/edit', function(req, res) {
    if(req.query.department_id == null) {
        res.send('A department id is required');
    }
    else {
        department_dal.edit(req.query.department_id, function(err, result) {
            res.render('department/departmentUpdate', {department: result[0]});
        });
    }
});

router.get('/update', function(req, res) {
    department_dal.update(req.query, function(err, result){
        res.redirect(302, '/department/all');
    });
});

// Delete an department for the given department_id
router.get('/delete', function(req, res){
    if(req.query.department_id == null) {
        res.send('department_id is null');
    }
    else {
        department_dal.delete(req.query.department_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/department/all');
            }
        });
    }
});

module.exports = router;