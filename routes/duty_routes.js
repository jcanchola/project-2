var express = require('express');
var router = express.Router();
var duty_dal = require('../model/duty_dal');

// View All duties
router.get('/all', function(req, res) {
    duty_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('duty/dutyViewAll', { 'result':result });
        }
    });

});

// View the duty for the given id
router.get('/', function(req, res){
    if(req.query.duty_id == null) {
        res.send('duty_id is null');
    }
    else {
        duty_dal.getById(req.query.duty_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('duty/dutyViewById', {'result': result});
            }
        });
    }
});

// Return the add a new duty form

router.get('/add', function(req, res) {
    res.render('duty/dutyAdd');
});

// View the duty for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.duty_name == null) {
        res.send('duty must be provided.');
    }
    else if(req.query.description == null) {
        res.send('Must enter description.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        duty_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it different once we start using Ajax
                res.redirect(302, '/duty/all');
            }
        });
    }
});

router.get('/edit', function(req, res) {
    if(req.query.duty_id == null) {
        res.send('A duty id is required');
    }
    else {
        duty_dal.edit(req.query.duty_id, function(err, result) {
            res.render('duty/dutyUpdate', {duty: result[0]});
        });
    }
});

router.get('/update', function(req, res) {
    caretaker_dal.update(req.query, function(err, result){
        res.redirect(302, '/caretaker/all');
    });
});

// Delete an department for the given department_id
router.get('/delete', function(req, res){
    if(req.query.duty_id == null) {
        res.send('duty_id is null');
    }
    else {
        duty_dal.delete(req.query.duty_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/duty/all');
            }
        });
    }
});

module.exports = router;